import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Manager from '@/components/Manager'
import User from '@/components/User'
import ManagerNewForm from '@/components/manager/ManagerNewForm'
import ManagerUpdateForm from '@/components/manager/ManagerUpdateForm'
import ManagerPlay from '@/components/manager/ManagerPlay'
import Connexion from '@/components/Connexion'
import Code from '@/components/Code'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/connexion',
      name: 'Connexion',
      component: Connexion
    },
    {
      path: '/manager',
      name: 'Manager',
      component: Manager,
    },
    {
      path: '/room/:roomid',
      name: 'User',
      component: User
    },
    {
      path: '/manager/new',
      name: 'new',
      component: ManagerNewForm
    },
    {
      path: '/manager/update/:id',
      name: 'update',
      component: ManagerUpdateForm
    },
    {
      path: '/manager/play/:id',
      name: 'play',
      component: ManagerPlay
    },
    {
      path: '/code',
      name: 'Code',
      component: Code
    }
  ]
})
