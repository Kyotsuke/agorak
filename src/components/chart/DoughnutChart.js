import { Doughnut, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Doughnut,
  mixins: [reactiveProp],
  props: ['options'],
  mounted () {
    // this.chartData est créé par le mixin.
    // si vous voulez transmettre des options, il faudra créer une variable locale
    this.renderChart(this.chartData, this.options)
  }
}